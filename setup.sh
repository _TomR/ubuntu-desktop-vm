# Fix locale so gnome-terminal will open
update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8

# Patch Ubuntu
apt-get update
apt-get upgrade -y

# Install development tools, git, hg, meld, Python virtualenv
apt-get install -y build-essential git mercurial meld virtualenv

# Install PyCharm
sh -c 'echo "deb http://archive.getdeb.net/ubuntu yakkety-getdeb apps" >> /etc/apt/sources.list.d/getdeb.list'
wget -q -O - http://archive.getdeb.net/getdeb-archive.key | apt-key add -
apt-get update
apt-get install pycharm -y
apt-get install pycharm -y --fix-missing & # For some reason, PyCharm itself consistently fails on first attempt?

wait %1
reboot