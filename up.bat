@echo off

set CURPATH=%cd%
set BINPATH=%~dp0

cd "%BINPATH%"
if not exist "./shared" mkdir "shared"
vagrant up %*

cd "%CURPATH%"