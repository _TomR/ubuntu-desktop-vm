# ubuntu-desktop-vm

A basic Python development environment under graphical Ubuntu 16.10, to be configured as a VM using `vagrant` + `VirtualBox`.

This VM is targeted at newbie programmers on Windows who just want to get started without spending a lot of time on configuration.  By choosing graphical Ubuntu, the environment should be practically familiar to Windows users while still providing the flexibility and compatibility of a UNIX-like shell.  Furthermore, the VM can be paused and resumed as-is at the user's convenience.

Notable inclusions:
 * `python2` and `python3`
 * `PyCharm` Python IDE.
 * `meld` for graphical diffs.
 * `build-essential` for standard tools like `gcc` and `make`.
 * `git` and `hg` for all everyone's DVCS needs.
 * A `shared` folder that links to `~/Desktop/shared` for exchanging files with the VM.

## Requirements
 1. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
 2. [Vagrant](https://www.vagrantup.com/downloads.html)

## Setup
In an Administrator command prompt:
```
git clone https://github.com/TomRichter/ubuntu-desktop-vm.git
cd ubuntu-desktop-vm
up.bat
```

Using Python's `virtualenv` within VirtualBox's shared folders from inside the VM will fail if `up.bat` or `vagrant up` is not run from an elevated command prompt.  This is due to Windows permissions and the creation of symlinks involved.

The VM may appear as if ready in the background of configuration, but please wait until `up.bat`'s console disappears and the VM reboots before using!